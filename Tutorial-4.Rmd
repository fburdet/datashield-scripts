---
title: "Tutorial 4 - Example of k-means clustering"
author: "Iulian Dragan, Mark Ibberson"
date: "5/2/2018"
output: pdf_document
---

# Aim of the tutorial

This tutorial shows an example of how to perform *k*-means clustering on two cohorts


## 1) Login to ANDIS and GoDARTS nodes


Log into the system as before.

```{r logon, echo=TRUE, warning=FALSE, message=FALSE, results='hide'}

# The usual incantations at the beginning of every datashield script:
library(datashieldclient)
library(dsCDISCclient)


logindata <- read.delim('//Users/mibber/Work/Projects/RHAPSODY/WP2/Rscripts_FEDDB/logindata_personal.txt')

logindata <- logindata[logindata$server %in% c('andis', 'godarts'),]

# logon to the nodes
opals <- datashield.login(logindata)

# assign LB (Laboratory measurement), VS (Vital signs), DM (Demographics), MH (Medical history) and CM (Concommitant medication) tables
# to remote dataframes on each node
datashield.assign(opals, 'lb', 'rhapsody.LB')
datashield.assign(opals, 'vs', 'rhapsody.VS')
datashield.assign(opals, 'dm', 'rhapsody.DM')
datashield.assign(opals, 'mh', 'rhapsody.MH')
datashield.assign(opals, 'cm', 'rhapsody.CM')

# convert the units for the LB and VS tables
ds2.convert.units('lb', datasources = opals)
ds2.convert.units('vs', datasources = opals)
  

```


## 2) Prepare the data for the 2 cohorts

Functions have already been coded to help with the preparation of data for clustering.
The data from each cohort needs to be prepared slightly differently because each cohort
recorded data in a slightly different way. For example, although the terms have been harmonised 
and the data converted to standard format (CDISC), different measurements are taken at baseline
and follow-up time-points in the different cohorts.
The functions for preparing the data are in a separate file which we will load.
The function for preparation of ANDIS data *prepare_cluster_andis* is commented to help you understand how it works.
The functions create a remote dataframe *kmeans_input* on the respective nodes.

```{r prepare, echo=TRUE, warning=FALSE, message=FALSE, results='hide'}

# load some functions
source("./Tutorial-4-functions.R")

# call the function for preparation of ANDIS data
prepare_cluster_andis()

# then for GoDARTS data
prepare_cluster_godarts()

# then for DCS data
#prepare_cluster_dcs()

# look at a summary of the k-means input dataframe
ds.summary('kmeans_input', datasources = opals)

```


## 3) Perform federated *k*-means clustering

The following code performs federated *k*-means clustering on ANDIS and GoDARTS cohorts:

```{r kmeans, echo=TRUE, warning=FALSE, message=FALSE, results='hide'}

cohorts <- c('andis','godarts')

# set the number of desired clusters
centers <- 5

# scale the input dataframe
kmeans.input <- 'kmeans_input'
kmeans.input.scaled <- paste0(kmeans.input, '_scaled')
ds2.scale(kmeans.input.scaled, kmeans.input, datasources = opals)

ds.summary('kmeans_input_scaled', datasources = opals)


# create factor and measurement names
cluster_factor <- paste0(kmeans.input.scaled, '_km_clust', centers)

cluster_measures <- ds2.colnames(kmeans.input.scaled, datasources = opals)

cluster_measures <- cluster_measures[[1]]

# perform the k-means clustering. This can take a few minutes as the algorithm performs 
# several iterations, recalculating cluster centers at each iteration
# If more than one cohort is provided the result will be for the combined cohorts
iter.max <- 30
nstart <- 30
ktot <- ds2.kmeans(kmeans.input.scaled, centers = centers, iter.max = iter.max, nstart = nstart, async = TRUE, datasources = opals)

# look at a summary of the results:
summary(ktot)
ktot


```


## 4) Make some plots of the results

There is a function to perform the *k*-means which formats the output for printing with another function.
The code for executing these function calls is below. The functions themselves are in the *Tutorial-4-functions.R* file.

```{r plot, echo=TRUE, warning=FALSE, message=FALSE, results='hide'}

# perform the clusterng
cl <- do_clustering(c('andis', 'godarts'), 5, iter.max = 30, nstart = 20)

# these are the cluster labels
cluster.labels <- c('Cluster1', 'Cluster2', 'Cluster3', 'Cluster4', 'Cluster5')

# call ggplot function and show plot in session
p <- do_ggplot(cl, cluster.labels, create.pdf = FALSE)

print(p)


# logout of the nodes to prevent zombies being created on the remote servers
datashield.logout(opals)

```



# Technical information

```{r echo=FALSE}

sessionInfo()

```

