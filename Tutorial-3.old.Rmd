---
title: "Tutorial 3 - Working with CDISC formatted data"
author: "Iulian Dragan, Mark Ibberson"
date: "5/2/2018"
output: pdf_document
---

# Aim of the tutorial

This tutorial showcases some of the most useful functions in dsCDISCclient that are adapted for working with CDISC SDTM formatted data such as:
1) Connecting to a local node and issuing local and remote commands
2) Subsetting dataframes
3) Data transformation from CDISC to wide format
4) Joining dataframes

CDISC SDTM is a format consisting of several 'domains' or tables:
DM:   Demographics
VS:   Vital signs
LB:   Laboratory measurements
MH:   Medical history
APMH: Associated persons medical history
CM:   Concommitant medication
SU:   Substance use

In this tutorial we will use the LB table containing the laboratory measurements as an example of how to work with CDISC formatted data using DataSHIELD.

## 1) Connecting to a local node and issuing local and remote commands
We will connect to a local node, in our case we will connect to 'ABOS' as this is hosted in Lausanne.

First, we need to connect directly to the Opal node (via ssh, or vnc or other means) and then can start R (or RStudio).
If you don't have access privileges to your local Opal node you need to ask the local person administering the node to create access for you.

Once connected, data can be loaded in the local R session. DataSHIELD and dsCDISCclient functions can be executed with the added advantage of being able to look directly at the results of each command which helps to understand what the different functions are doing. For example, str() and head() R functions can be used to inspect the results objects.
***NB. This script will not work on a normal remote client, it is designed to run only on if logged into a local node***

The commands below connect to the local node:
```{r connect, include=TRUE}

# The usual incantations at the beginning of every datashield script:
library(datashieldclient)
library(dsCDISCclient)


# This session will contain code and objects for both the client and the server side, this is why I need to load the 
# server libraries as well (this is not necessary in a normal, remote session)
library(datashield)
library(dsCDISC)

# Slightly modified call to datashield.login (no logindata dataframe)
opals <- datashield.login(c(local = 1)) # start one session (could be more than one but this is a story for another tutorial)
opals

```


Now, create a copy of the LB dataframe containing laboratory measurements in the local R session using *datashield.assign*. Since we are logged onto the local Opal machine we can 'see' this dataframe and use normal R commands such as str() and head() to inspect it.

```{r assign, include=TRUE}

# load the LB table:
datashield.assign(opals, 'lb', 'rhapsody.LB')

# data is stored in CDISC format - a key/value format
head(lb[order(lb$SUBJID),],20)
str(lb)
```

We can execute DataSHIELD methods that are normally used only for remote function calls on the local data.
For example, we can look at a summary of the local dataframe and show factors.

```{r datashield methods, include=TRUE}
# 'remote' datashield methods work on the local data; this can be useful for creating and debugging scripts.
# all the ds/datashield/ds2 functions that follow can be executed equally on the remote nodes in a 'normal' session.
# here are 2 of the most useful ones:
ds.summary('lb', datasources = opals)
ds2.show.factors('lb', datasources = opals)

```

One important step after loading the data from LB or VS (Vital signs) tables is to convert the units. There is a function *ds2.convert.units* to do this automatically.
It works by converting source units to standardized units so that data are the same in all cohorts.
The following code also includes the command to see the conversion table that the function uses:

```{r unit conversion, include=TRUE}

# convert the units
ds2.convert.units('lb', datasources = opals)

# ds2.convert.units uses a conversion data frame that is part of the dsCDISC package:
dsCDISC:::conversion
# remember we are still local, meaning all data is present and visible in the session. The above command would not work in a normal, remote session because the package dsCDISC (server side) would not be loaded

```


## 2) Subsetting dataframes

Subsetting of data frames will normally be required before any analysis is possible. Subsetting is filtering the data based on one or more rules. In the code below we will subset the LB data frame to obtain only HBA1C results using the function *ds2.subset()*. The CDISC variable that contains the code for the measurement is called 'LBTESTCD'. The variable 'LBTEST' contains the measurement name (this could also have been used for filtering). Because we are logged on to the local Opal server we can look at the results of the subsetting directly (for example using *head()* or *str()*), which is normally not possible when connecting to a remote Opal server.
The way *ds2.subset()* works is to apply a row filter and/or column filter to the remote dataframe. The row/column filter is basically what you would put in a normal R subsetting command, e.g. *my.rowfilter.example <- my.dataframe[LBTESTCD == "HBA1C",]* or *my.colfilter.example <- my.dataframe[,colnames(my.dataframe) %in% c('SUBJID','LBTEST','LBORRES','LBORRESU')]*. *LBORRES* and *LBORRESU* are the measurement and measurement unit columns respectively.
The code below contains examples of row and column filtering and introduces the function *ds2.is.unique* and how to make a copy of the dataframe (this can be done by setting *row.filter* or *col.filter* to 'TRUE').

```{r subset, include=TRUE}

ds2.subset(symbol = 'hba1c', what = 'lb', row.filter = 'LBTESTCD == "HBA1C"', datasources = opals)
hba1c_local <- lb[lb$LBTESTCD == "HBA1C",]
str(hba1c)
str(hba1c_local)
# ds2.subset differs from the normal subsetting technique in that it recreates the factor levels to show only the ones present in the result 
# (see for instance hba1c$LBTESTCD).
# this is useful because  we can look at the factors and see only what's left in the subset:
ds2.show.factors('hba1c', datasources = opals)
# the row.filter and col.filter parameters can take *any* expression that would work normally in the square bracket subsetting method,
# including arbitrary code blocks enclosed in braces (more about blocks in the next script)
# a few examples :
#   - complete cases + column subset:
ds2.subset('lb_complete', 'lb', row.filter = 'complete.cases(lb)', col.filter = '!grepl("TEST", colnames(lb))', datasources = opals)
str(lb_complete)
head(lb_complete)

#   - remove duplicates
ds2.subset('lb_uniq', 'lb', row.filter = '!duplicated(SUBJID)' , datasources = opals)
is.unique(lb_uniq$SUBJID)

# introducing ds2.is.unique:
ds2.is.unique('lb_uniq$SUBJID', datasources = opals)
#   - order by a column:
ds2.subset('hba1c_ordered', 'hba1c', row.filter = 'order(SUBJID)' , datasources = opals)
head(hba1c_ordered,20)

#   - copy a data frame 
ds2.subset('lb_twin', 'lb', row.filter = 'TRUE', datasources = opals)
identical(lb_twin, lb)

```


It can be useful to subset a dataframe according to a set of variables e.g. creating separate dataframes for each *VISIT* or each *LBTESTCD*.
For this a function has been implemented called *ds2.subsetByClass* to which you provide the dataframe columns that you want to subset by. The function will create a new dataframe, one for each variable in each column.
If 2 columns are provided, a new dataframe is generated for each combination of variables in the 2 columns. The results are output as a list of dataframes.
In the example below subsetting is performed according to *VISIT* and then *VISIT + LBTESTCD*.

```{r subset by class, include=TRUE}

# Another useful subsetting method - by class:
head(lb)
ds2.subsetByClass(x = 'lb', subsets = 'by_visit' , variables = 'lb$VISIT', datasources = opals )
str(by_visit)

# Can use more than one variable column (class):
ds2.subsetByClass('lb', subsets = 'by_visit_test' , variables = c('lb$VISIT','lb$LBTESTCD'), datasources = opals )
names(by_visit_test)
str(by_visit_test[1])

```

CDISC is in long format meaning that there are multiple lines for each subject. Sometimes you may want to *widen* the long format data to a format where you have one line per subject and each variable is a column.
In the code below we take some data subsetted by visit and measurement and widen this using the function *ds2.pivot*. There is an additional function *ds2.suggest.pivot* to help you. This function suggests a formula for widening the dataframe.

```{r widening, include=TRUE}

#is it unique?
ds2.is.unique('by_visit_test$BASELINE.CPEPTIDE$SUBJID')

# we still have multiple lines per SUBJID. Let's try to obtain a wide, unique result.
# we use reshape2::dcast for widening

# ask for a widening formula suggestion:
ds2.suggest.pivot('by_visit_test$BASELINE.CPEPTIDE')
# the terms on the left hand side don't vary for the same SUBJID so we can ignore them safely. 
# On the right hand we know that LBTPT and LBTPTNUM are the same, so: 

ds2.pivot('wide_cpeptide_baseline', 'by_visit_test$BASELINE.CPEPTIDE', value.var = 'LBORRES', formula ='SUBJID ~ LBTPTNUM' , completeCases = TRUE)
ds2.is.unique('wide_cpeptide_baseline$SUBJID')
ds.summary('wide_cpeptide_baseline')

```

Another important tool is the ability to be able to join two dataframes using a common identifier (normally this will be the *SUBJID*). In the code below we join two dataframes together by subject id: one containing *CPEPTIDE* measurements and the other containing *HBA1C* measurements. The function to perform the joining is called *ds2.join*. There is also an example of how to rename columns on a remote dataframe using *ds2.colnames*.
Normally for the joining we want to perform an *inner* join. This means that only rows from table 1 (*wide_hba1c_baseline* below) that match table 2 (*wide_cpeptide_baseline*) will be returned. All columns from both tables will be present in the output dataframe. *ds2.join* inherits from the R *join* function meaning that the same join types can be specified in the two functions.

```{r joining, include=TRUE}

# We have one dataframe with unique measurements of CPEPTIDE, let's create another one for, say, HBA1C and then join the 2
# Repeat the above process for HBA1C. We already have our subsets in the list by_vist_test
ds.names('by_visit_test', datasources = opals)
ds.summary('by_visit_test$BASELINE.HBA1C', datasources = opals)
ds2.is.unique('by_visit_test$BASELINE.HBA1C$SUBJID', datasources = opals)
# The dataframe contains already unique measurements so no need for widening.

# Let's just copy it under a new name and keep only the interesting columns 
ds2.subset(symbol = 'wide_hba1c_baseline', what = 'by_visit_test$BASELINE.HBA1C', row.filter = 'TRUE', col.filter = 'c("SUBJID", "LBORRES")')
ds.summary('wide_hba1c_baseline') # if datasources is not specified, it's taken from the environment
ds2.colnames('wide_hba1c_baseline', c('SUBJID', 'HBA1C'))
ds.summary('wide_hba1c_baseline')

# while we are at it, rename also the columns of the cpeptide dataframe
ds2.colnames('wide_cpeptide_baseline', c('SUBJID', 'CPEPT_0', 'CPEPT_120'))
ds.summary('wide_cpeptide_baseline')

# join the two dataframes:
ds2.join(what = c('wide_hba1c_baseline', 'wide_cpeptide_baseline'), symbol = 'final', by= 'SUBJID', join.type = 'inner')
ds.summary('final')
ds2.is.unique('final$SUBJID')
head(final, 20)


```

Finally, it's worth mentioning that DataSHEILD functions exist that test for the presence of NAs in your data. However these functions work a bit differently from standard R functions such as *is.na()*.
There are 2 functions: *ds.isNA()* which returns true if there are *only* NAs in the specified column or row (i.e. this function is useful for detecting and removing NA columns or rows); and *ds.numNA* which counts the number of NAs.

```{r NAs, include=TRUE}
#NAs
#ds.isNA returns TRUE only if the column contains only NAs. ds.numNA counts the number of NAs
ds.isNA('lb$LBTPT')
any(is.na(lb$LBTPT))
ds.numNA('lb$LBTPT')

# logout of the nodes to prevent zombies being created on the remote servers
datashield.logout(opals)

```

That's it! Now you are ready for Tutorial 4!


# Technical information

```{r echo=FALSE}

sessionInfo()

```

