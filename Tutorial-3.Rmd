---
title: "Tutorial 3 - Federated PCA"
author: "Iulian Dragan, Mark Ibberson"
date: "11/11/2019"
output: pdf_document
---

# Aim of the tutorial

This tutorial demonstrates how you can perform federated PCA remotely.
We will use a standard R dataset *Iris* to do this.



## 1) Run standard PCA on the *Iris* dataset

We will start by performing a standard PCA on the *Iris* dataset using the *prcomp* function:

```{r standard pca, echo=TRUE, warning=FALSE, message=FALSE, results='hide'}

data('iris')
str(iris)

local.prcomp <- prcomp(iris[,1:4])
summary(local.prcomp)

```

## 2) Federated PCA

In order to show how the federated PCA works we will partition the *Iris* data onto two nodes.
We will first connect to two federated nodes and then create partitioned dataframes using the *Iris* dataset.
This works because *Iris* is included in the base package installation of R, so each node server has this data already.

```{r partition, echo=TRUE, warning=FALSE, message=FALSE, results='hide'}

# load the required packages
library(datashieldclient)
library(dsCDISCclient)


logindata <- read.delim('//Users//iulian//Rhapsody//logindata_proxy.txt')
#connect to 2 nodes
opals <- datashield.login(logindata[logindata$server %in% c('abos', 'colaus'),])

# load 2 partitions of iris on the 2 nodes:
# the partial.data function was written for testing purposes 
#and works by calling the R 'data' function
# each of the partial datasets is called 'iris' on the two nodes
datashield.aggregate(opals[1], as.symbol('partial.data("iris", 1, 74)'))
datashield.aggregate(opals[2], as.symbol('partial.data("iris", 75, 150)'))

# what's been loaded?
x <- ds.summary('iris')
str(x)

```

Now that the data have been partitioned onto two nodes, we can try the PCA.
We can run federated PCA in two modes: *combined* mode where the datasets on different nodes are combined *virtually*;
and *split* mode where the PCA is performed separately on each cohort dataset.

We have the *Iris* data split into two, one part on the CoLaus node, the other on the Abos node.
Federated PCA uses an adapted version of the *princomp* function in R.
The results of the PCA are returned as a list of PCA objects:
When we run the PCA in *combined* mode the list element name is called 'global'
When run in *split* mode the list element name is the name of the cohort where the data resides.

Run the code below and compare the results of the federated PCA run in *combined* mode
with the results obtained in (1) *local.prcomp*

```{r federated pca, echo=TRUE, warning=FALSE, message=FALSE, results='hide'}

# execute remote princomp on the virtual iris data frame:
my.pca <- ds2.princomp('iris', type='combine', datasources = opals)
#check the results:
my.pca
summary(my.pca$global)

my.pca2 <- ds2.princomp('iris', type='split', datasources = opals)
#check the results:
my.pca2

summary(my.pca2$abos)
summary(my.pca2$colaus)


```

## 3) Plotting PCA results

Results of federated PCA can be plotted similarly to normal PCA except that the individual points cannot be viewed.
The way around this is to use *smooth scatterplots*. This is necessary because it could be possible to get back to the 
original data from the points projected by PCA.
The code below shows biplots from the original local PCA alongside the smooth scatterplot from the federated PCA

```{r pca plots1, echo=TRUE, warning=FALSE, message=FALSE, results='hide'}

# first the proportion of variances
par(mfrow = c(1,2))
plot(local.prcomp)
plot(my.pca$global)


biplot(local.prcomp, xlabs = rep('0', 150))
biplot(my.pca$global)

datashield.logout(opals)

```


The code below performs a PCA split between 3 nodes and then plots the results as a smooth scatterplot.
In this example, the original *iris* data are copied to a remote dataframe *iris_scores* which are used
to colour the smooth scatterplot according to plant species.
This demonstrates how you can display different groups in a PCA by colour.

```{r pca plots2, echo=TRUE, warning=FALSE, message=FALSE, results='hide'}

# Same happens with any number of nodes, let's try with 3:
opals <- datashield.login(logindata[logindata$server %in% c('abos', 'colaus', 'andis'),])

datashield.aggregate(opals[1], as.symbol('partial.data("iris", 1, 40)'))
datashield.aggregate(opals[2], as.symbol('partial.data("iris", 41, 100)'))
datashield.aggregate(opals[3], as.symbol('partial.data("iris", 101, 150)'))

x <- ds.summary('iris')
str(x)

#ds.summary('iris')
my.pca <- ds2.princomp('iris', type='combine', scores.suffix = "_scores")
summary(my.pca$global)

ds.summary('iris_scores')

#iris_scores contains also the Species column, we can categorize by it:
par(mfrow = c(1,1))
biplot(my.pca$global, levels = 'iris_scores$Species', npoints = 256, shades = 16)


datashield.logout(opals)


```



That's it! Now you are ready for Tutorial 3!


# Technical information

```{r echo=FALSE}

sessionInfo()

```

